/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucr.ac.cr.modelo;

/**
 *
 * @author aaron
 */
public class Estudio {
    
    private String id;
    private String descripcion;

    public Estudio() {
        this.id = "";
        this.descripcion = "";
    }

    public Estudio(String id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Estudio{" + "id=" + id + ", descripcion=" + descripcion + '}';
    }
    
    
}
