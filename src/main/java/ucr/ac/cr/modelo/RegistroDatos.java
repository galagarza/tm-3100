/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucr.ac.cr.modelo;

/**
 *
 * @author aaron
 */
public interface RegistroDatos {

    public abstract String agregar(Object obj);

    public abstract String eliminar(Object obj);

    public abstract Object buscar(String id);

    public abstract String toString();
}
