/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucr.ac.cr.modelo;

import java.util.ArrayList;

/**
 *
 * @author aaron
 */
public class RegistroEstudio implements RegistroDatos {

    private ArrayList<Estudio> listaEstudios;

    public RegistroEstudio() {
        this.listaEstudios = new ArrayList<Estudio>();
    }

    @Override
    public String agregar(Object obj) {
        if (obj != null) {
            Estudio estudio = (Estudio) obj;
            if (this.buscar(estudio.getId()) == null) {
                this.listaEstudios.add(estudio);
                return "Estudio agregado con éxito!";
            }
            return "El estudio ya se encuentra registrado";
        }
        return "Error al registrar el estudio.";
    }

    @Override
    public String eliminar(Object estudio) {
       
        if (estudio != null) {
            this.listaEstudios.remove((Estudio)estudio);
            return "Estudio eliminado con éxito.";
        }
        return "Error al eliminar estudio!";
    }

    @Override
    public Object buscar(String id) {
        for (int i = 0; i < this.listaEstudios.size(); i++) {
            if (this.listaEstudios.get(i).getId().equalsIgnoreCase(id)) {
                return (Object) this.listaEstudios.get(i);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String salida = "Lista de estudios:\n";
        for (Estudio estudio : listaEstudios) {
            salida += estudio + "\n";
        }
        return salida;
    }

}
